package ru.t1.amsmirnov.taskmanager.component;

import ru.t1.amsmirnov.taskmanager.api.controller.ICommandController;
import ru.t1.amsmirnov.taskmanager.api.controller.IProjectController;
import ru.t1.amsmirnov.taskmanager.api.controller.IProjectTaskController;
import ru.t1.amsmirnov.taskmanager.api.controller.ITaskController;
import ru.t1.amsmirnov.taskmanager.api.repository.ICommandRepository;
import ru.t1.amsmirnov.taskmanager.api.repository.IProjectRepository;
import ru.t1.amsmirnov.taskmanager.api.repository.ITaskRepository;
import ru.t1.amsmirnov.taskmanager.api.service.ICommandService;
import ru.t1.amsmirnov.taskmanager.api.service.IProjectService;
import ru.t1.amsmirnov.taskmanager.api.service.IProjectTaskService;
import ru.t1.amsmirnov.taskmanager.api.service.ITaskService;
import ru.t1.amsmirnov.taskmanager.constant.ArgumentConst;
import ru.t1.amsmirnov.taskmanager.constant.CommandConst;
import ru.t1.amsmirnov.taskmanager.controller.CommandController;
import ru.t1.amsmirnov.taskmanager.controller.ProjectController;
import ru.t1.amsmirnov.taskmanager.controller.ProjectTaskController;
import ru.t1.amsmirnov.taskmanager.controller.TaskController;
import ru.t1.amsmirnov.taskmanager.enumerated.Status;
import ru.t1.amsmirnov.taskmanager.exception.AbstractException;
import ru.t1.amsmirnov.taskmanager.exception.system.ArgumentNotSupportedException;
import ru.t1.amsmirnov.taskmanager.exception.system.CommandNotSupportedException;
import ru.t1.amsmirnov.taskmanager.model.Project;
import ru.t1.amsmirnov.taskmanager.repository.CommandRepository;
import ru.t1.amsmirnov.taskmanager.repository.ProjectRepository;
import ru.t1.amsmirnov.taskmanager.repository.TaskRepository;
import ru.t1.amsmirnov.taskmanager.service.CommandService;
import ru.t1.amsmirnov.taskmanager.service.ProjectService;
import ru.t1.amsmirnov.taskmanager.service.ProjectTaskService;
import ru.t1.amsmirnov.taskmanager.service.TaskService;
import ru.t1.amsmirnov.taskmanager.util.TerminalUtil;

public final class Bootstrap {

    private final ICommandRepository commandRepository = new CommandRepository();
    private final IProjectRepository projectRepository = new ProjectRepository();
    private final ITaskRepository taskRepository = new TaskRepository();

    private final ICommandService commandService = new CommandService(commandRepository);
    private final IProjectService projectService = new ProjectService(projectRepository);
    private final ITaskService taskService = new TaskService(taskRepository);
    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    private final ICommandController commandController = new CommandController(commandService);
    private final ITaskController taskController = new TaskController(taskService);
    private final IProjectController projectController = new ProjectController(projectService, projectTaskService);
    private final IProjectTaskController projectTaskController = new ProjectTaskController(projectTaskService);

    private void initDemoData() {
        try {
            projectService.add(new Project("Project 1", "Description 1", Status.IN_PROGRESS));
            projectService.add(new Project("Project 2", "Description 2", Status.NOT_STARTED));
            projectService.add(new Project("Project 3", "Description 3", Status.COMPLETED));

            taskService.createTask("TASK 1", "TASK 1");
            taskService.createTask("TASK 2", "TASK 2");
        } catch (Exception exception) {
            renderError("Error while initializing demo data! " + exception.getMessage());
        }
    }

    private void renderError(final String message) {
        System.out.println(message);
        System.out.println("[FAIL]");
    }

    public void start(String[] args) {
        if (processArguments(args)) exit();

        initDemoData();
        commandController.showWelcome();
        while (!Thread.currentThread().isInterrupted()) {
            try {
                System.out.println("ENTER COMMAND: ");
                final String command = TerminalUtil.nextLine();
                processCommand(command);
                System.out.println("[OK]");
            } catch (final Exception exception) {
                renderError(exception.getMessage());
            }
        }
    }

    public boolean processArguments(final String[] args) {
        if (args == null || args.length == 0) return false;
        final String argument = args[0];
        try {
            processArgument(argument);
        } catch (Exception exception) {
            System.out.println(exception.getMessage());
        }
        return true;
    }

    public void processArgument(final String argument) throws AbstractException {
        switch (argument) {
            case ArgumentConst.HELP:
                commandController.showHelp();
                break;
            case ArgumentConst.ABOUT:
                commandController.showAbout();
                break;
            case ArgumentConst.VERSION:
                commandController.showVersion();
                break;
            case ArgumentConst.INFO:
                commandController.showInfo();
                break;
            case ArgumentConst.ARGUMENTS:
                commandController.showArguments();
                break;
            case ArgumentConst.COMMANDS:
                commandController.showCommands();
                break;
            default:
                throw new ArgumentNotSupportedException(argument);
        }
    }

    public void processCommand(final String command) throws AbstractException {
        switch (command) {
            case CommandConst.HELP:
                commandController.showHelp();
                break;
            case CommandConst.ABOUT:
                commandController.showAbout();
                break;
            case CommandConst.VERSION:
                commandController.showVersion();
                break;
            case CommandConst.INFO:
                commandController.showInfo();
                break;
            case CommandConst.ARGUMENTS:
                commandController.showArguments();
                break;
            case CommandConst.COMMANDS:
                commandController.showCommands();
                break;
            case CommandConst.PROJECT_LIST:
                projectController.showProjects();
                break;
            case CommandConst.PROJECT_CLEAR:
                projectController.clearProjects();
                break;
            case CommandConst.PROJECT_CREATE:
                projectController.createProject();
                break;
            case CommandConst.PROJECT_SHOW_BY_ID:
                projectController.showProjectById();
                break;
            case CommandConst.PROJECT_SHOW_BY_INDEX:
                projectController.showProjectByIndex();
                break;
            case CommandConst.PROJECT_UPDATE_BY_ID:
                projectController.updateProjectById();
                break;
            case CommandConst.PROJECT_UPDATE_BY_INDEX:
                projectController.updateProjectByIndex();
                break;
            case CommandConst.PROJECT_CHANGE_STATUS_BY_ID:
                projectController.changeProjectStatusById();
                break;
            case CommandConst.PROJECT_CHANGE_STATUS_BY_INDEX:
                projectController.changeProjectStatusByIndex();
                break;
            case CommandConst.PROJECT_START_BY_ID:
                projectController.startProjectById();
                break;
            case CommandConst.PROJECT_START_BY_INDEX:
                projectController.startProjectByIndex();
                break;
            case CommandConst.PROJECT_COMPLETE_BY_ID:
                projectController.completeProjectById();
                break;
            case CommandConst.PROJECT_COMPLETE_BY_INDEX:
                projectController.completeProjectByIndex();
                break;
            case CommandConst.PROJECT_REMOVE_BY_ID:
                projectController.removeProjectById();
                break;
            case CommandConst.PROJECT_REMOVE_BY_INDEX:
                projectController.removeProjectByIndex();
                break;
            case CommandConst.TASK_LIST:
                taskController.showTasks();
                break;
            case CommandConst.TASK_CLEAR:
                taskController.clearTasks();
                break;
            case CommandConst.TASK_CREATE:
                taskController.createTask();
                break;
            case CommandConst.TASK_SHOW_BY_ID:
                taskController.showTaskById();
                break;
            case CommandConst.TASK_SHOW_BY_INDEX:
                taskController.showTaskByIndex();
                break;
            case CommandConst.TASK_SHOW_BY_PROJECT_ID:
                taskController.showTaskByProjectId();
                break;
            case CommandConst.TASK_UPDATE_BY_ID:
                taskController.updateTaskById();
                break;
            case CommandConst.TASK_UPDATE_BY_INDEX:
                taskController.updateTaskByIndex();
                break;
            case CommandConst.TASK_CHANGE_STATUS_BY_ID:
                taskController.changeTaskStatusById();
                break;
            case CommandConst.TASK_CHANGE_STATUS_BY_INDEX:
                taskController.changeTaskStatusByIndex();
                break;
            case CommandConst.TASK_START_BY_ID:
                taskController.startTaskById();
                break;
            case CommandConst.TASK_START_BY_INDEX:
                taskController.startTaskByIndex();
                break;
            case CommandConst.TASK_COMPLETE_BY_ID:
                taskController.completeTaskById();
                break;
            case CommandConst.TASK_COMPLETE_BY_INDEX:
                taskController.completeTaskByIndex();
                break;
            case CommandConst.TASK_BIND_TO_PROJECT:
                projectTaskController.bindTaskToProject();
                break;
            case CommandConst.TASK_UNBIND_FROM_PROJECT:
                projectTaskController.unbindTaskToProject();
                break;
            case CommandConst.TASK_REMOVE_BY_ID:
                taskController.removeTasksById();
                break;
            case CommandConst.TASK_REMOVE_BY_INDEX:
                taskController.removeTasksByIndex();
                break;
            case CommandConst.EXIT:
                exit();
                break;
            default:
                throw new CommandNotSupportedException(command);
        }
    }

    public void exit() {
        System.exit(0);
    }

}
