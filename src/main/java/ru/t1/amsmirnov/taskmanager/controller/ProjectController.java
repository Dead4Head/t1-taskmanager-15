package ru.t1.amsmirnov.taskmanager.controller;

import ru.t1.amsmirnov.taskmanager.api.controller.IProjectController;
import ru.t1.amsmirnov.taskmanager.api.service.IProjectService;
import ru.t1.amsmirnov.taskmanager.api.service.IProjectTaskService;
import ru.t1.amsmirnov.taskmanager.enumerated.Sort;
import ru.t1.amsmirnov.taskmanager.enumerated.Status;
import ru.t1.amsmirnov.taskmanager.exception.AbstractException;
import ru.t1.amsmirnov.taskmanager.model.Project;
import ru.t1.amsmirnov.taskmanager.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public final class ProjectController implements IProjectController {

    private final IProjectService projectService;
    private final IProjectTaskService projectTaskService;

    public ProjectController(final IProjectService projectService, final IProjectTaskService projectTaskService) {
        this.projectService = projectService;
        this.projectTaskService = projectTaskService;
    }

    private void showProject(final Project project) {
        if (project == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("ID: " + project.getId());
        System.out.println("NAME: " + project.getName());
        System.out.println("DESCRIPTION: " + project.getDescription());
        System.out.println("STATUS: " + Status.toName(project.getStatus()));
    }

    @Override
    public void createProject() throws AbstractException {
        System.out.println("[PROJECT CREATE]");
        System.out.println("ENTER PROJECT NAME:");
        final String projectName = TerminalUtil.nextLine();
        System.out.println("ENTER PROJECT DESCRIPTION:");
        final String projectDescription = TerminalUtil.nextLine();
        projectService.create(projectName, projectDescription);
    }

    @Override
    public void showProjects() {
        System.out.println("[PROJECT LIST]");
        System.out.println("ENTER SORT: ");
        System.out.println(Arrays.toString(Sort.values()));
        final String sortType = TerminalUtil.nextLine();
        final Sort sort = Sort.toSort(sortType);
        final List<Project> projects = projectService.findAll(sort);
        int index = 1;
        for (final Project project : projects) {
            if (project == null) continue;
            System.out.println(index + ". " + project.getName());
            index++;
        }
    }

    @Override
    public void showProjectById() throws AbstractException {
        System.out.println("[SHOW PROJECT BY ID]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        Project project = projectService.findOneById(id);
        showProject(project);
    }

    @Override
    public void showProjectByIndex() throws AbstractException {
        System.out.println("[SHOW PROJECT BY INDEX]");
        System.out.println("ENTER INDEX: ");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Project project = projectService.findOneByIndex(index);
        showProject(project);
    }

    @Override
    public void updateProjectById() throws AbstractException {
        System.out.println("[UPDATE PROJECT BY ID]");
        System.out.println("ENTER ID: ");
        final String id = TerminalUtil.nextLine();
        System.out.println("ENTER NAME: ");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION: ");
        final String description = TerminalUtil.nextLine();
        projectService.updateById(id, name, description);
    }

    @Override
    public void updateProjectByIndex() throws AbstractException {
        System.out.println("[UPDATE PROJECT BY INDEX]");
        System.out.println("ENTER INDEX: ");
        final Integer index = TerminalUtil.nextNumber() - 1;
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION: ");
        final String description = TerminalUtil.nextLine();
        projectService.updateByIndex(index, name, description);
    }

    @Override
    public void changeProjectStatusById() throws AbstractException {
        System.out.println("[CHANGE PROJECT STATUS BY ID]");
        System.out.println("ENTER ID: ");
        final String id = TerminalUtil.nextLine();
        System.out.println("ENTER STATUS: ");
        final String statusValue = TerminalUtil.nextLine();
        final Status status = Status.toStatus(statusValue);
        if (status == null) {
            System.out.println("[FAIL]");
            return;
        }
        projectService.changeStatusById(id, status);
    }

    @Override
    public void changeProjectStatusByIndex() throws AbstractException {
        System.out.println("[CHANGE PROJECT STATUS BY INDEX]");
        System.out.println("ENTER INDEX: ");
        final Integer index = TerminalUtil.nextNumber() - 1;
        System.out.println("ENTER STATUS: ");
        final String statusValue = TerminalUtil.nextLine();
        final Status status = Status.toStatus(statusValue);
        projectService.changeStatusByIndex(index, status);
    }

    @Override
    public void startProjectById() throws AbstractException {
        System.out.println("[START PROJECT BY ID]");
        System.out.println("ENTER ID: ");
        final String id = TerminalUtil.nextLine();
        projectService.changeStatusById(id, Status.IN_PROGRESS);
    }

    @Override
    public void startProjectByIndex() throws AbstractException {
        System.out.println("[START PROJECT BY INDEX]");
        System.out.println("ENTER INDEX: ");
        final Integer index = TerminalUtil.nextNumber() - 1;
        projectService.changeStatusByIndex(index, Status.IN_PROGRESS);
    }

    @Override
    public void completeProjectById() throws AbstractException {
        System.out.println("[COMPLETE PROJECT BY ID]");
        System.out.println("ENTER ID: ");
        final String id = TerminalUtil.nextLine();
        projectService.changeStatusById(id, Status.COMPLETED);
    }

    @Override
    public void completeProjectByIndex() throws AbstractException {
        System.out.println("[COMPLETE PROJECT BY INDEX]");
        System.out.println("ENTER INDEX: ");
        final Integer index = TerminalUtil.nextNumber() - 1;
        projectService.changeStatusByIndex(index, Status.COMPLETED);
    }

    @Override
    public void clearProjects() {
        System.out.println("[CLEAR PROJECTS LIST]");
        projectService.clear();
    }

    @Override
    public void removeProjectById() throws AbstractException {
        System.out.println("[REMOVE PROJECT BY ID]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        final Project project = projectService.findOneById(id);
        projectTaskService.removeProjectById(project.getId());
    }

    @Override
    public void removeProjectByIndex() throws AbstractException {
        System.out.println("[REMOVE PROJECT BY INDEX]");
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Project project = projectService.findOneByIndex(index);
        projectTaskService.removeProjectById(project.getId());
    }

}
