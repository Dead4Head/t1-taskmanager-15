package ru.t1.amsmirnov.taskmanager.controller;

import ru.t1.amsmirnov.taskmanager.api.controller.ITaskController;
import ru.t1.amsmirnov.taskmanager.api.service.ITaskService;
import ru.t1.amsmirnov.taskmanager.enumerated.Sort;
import ru.t1.amsmirnov.taskmanager.enumerated.Status;
import ru.t1.amsmirnov.taskmanager.exception.AbstractException;
import ru.t1.amsmirnov.taskmanager.model.Task;
import ru.t1.amsmirnov.taskmanager.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public final class TaskController implements ITaskController {

    private final ITaskService taskService;

    public TaskController(final ITaskService taskService) {
        this.taskService = taskService;
    }

    private void showTask(final Task task) {
        if (task == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("ID: " + task.getId());
        System.out.println("NAME: " + task.getName());
        System.out.println("DESCRIPTION: " + task.getDescription());
        System.out.println("STATUS: " + Status.toName(task.getStatus()));
    }

    private void renderTasks(final List<Task> taskList) {
        if (taskList == null) {
            System.out.println("[FAIL]");
            return;
        }
        int index = 1;
        for (final Task task : taskList) {
            System.out.println(index + ". " + task.getName());
            index++;
        }
    }

    @Override
    public void createTask() throws AbstractException {
        System.out.println("[TASK CREATE]");
        System.out.println("ENTER TASK NAME:");
        final String taskName = TerminalUtil.SCANNER.nextLine();
        System.out.println("ENTER TASK DESCRIPTION:");
        final String taskDescription = TerminalUtil.SCANNER.nextLine();
        taskService.createTask(taskName, taskDescription);
    }

    @Override
    public void showTasks() {
        System.out.println("[TASK LIST]");
        System.out.println("ENTER SORT:");
        System.out.println(Arrays.toString(Sort.values()));
        final String sortType = TerminalUtil.nextLine();
        final Sort sort = Sort.toSort(sortType);
        final List<Task> taskList = taskService.findAll(sort);
        renderTasks(taskList);
    }

    @Override
    public void showTaskById() throws AbstractException {
        System.out.println("[SHOW TASK BY ID]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        final Task task = taskService.findOneById(id);
        showTask(task);
    }

    @Override
    public void showTaskByIndex() throws AbstractException {
        System.out.println("[SHOW TASK BY INDEX]");
        System.out.println("ENTER INDEX: ");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Task task = taskService.findOneByIndex(index);
        showTask(task);
    }

    @Override
    public void showTaskByProjectId() throws AbstractException {
        System.out.println("[SHOW TASK BY PROJECT ID]");
        System.out.println("ENTER ID: ");
        final String projectId = TerminalUtil.nextLine();
        final List<Task> tasks = taskService.findAllByProjectId(projectId);
        renderTasks(tasks);
    }

    @Override
    public void updateTaskById() throws AbstractException {
        System.out.println("[UPDATE TASK BY ID]");
        System.out.println("ENTER ID: ");
        final String id = TerminalUtil.nextLine();
        System.out.println("ENTER NAME: ");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION: ");
        final String description = TerminalUtil.nextLine();
        taskService.updateById(id, name, description);
    }

    @Override
    public void updateTaskByIndex() throws AbstractException {
        System.out.println("[UPDATE TASK BY INDEX]");
        System.out.println("ENTER INDEX: ");
        final Integer index = TerminalUtil.nextNumber() - 1;
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION: ");
        final String description = TerminalUtil.nextLine();
        taskService.updateByIndex(index, name, description);
    }

    @Override
    public void changeTaskStatusById() throws AbstractException {
        System.out.println("[CHANGE TASK STATUS BY ID]");
        System.out.println("ENTER ID: ");
        final String id = TerminalUtil.nextLine();
        System.out.println("ENTER STATUS: ");
        final String statusValue = TerminalUtil.nextLine();
        final Status status = Status.toStatus(statusValue);
        taskService.changeStatusById(id, status);
    }

    @Override
    public void changeTaskStatusByIndex() throws AbstractException {
        System.out.println("[CHANGE TASK STATUS BY INDEX]");
        System.out.println("ENTER INDEX: ");
        final Integer index = TerminalUtil.nextNumber() - 1;
        System.out.println("ENTER STATUS: ");
        final String statusValue = TerminalUtil.nextLine();
        final Status status = Status.toStatus(statusValue);
        final Task task = taskService.changeStatusByIndex(index, status);
    }

    @Override
    public void startTaskById() throws AbstractException {
        System.out.println("[START TASK BY ID]");
        System.out.println("ENTER ID: ");
        final String id = TerminalUtil.nextLine();
        taskService.changeStatusById(id, Status.IN_PROGRESS);
    }

    @Override
    public void startTaskByIndex() throws AbstractException {
        System.out.println("[START TASK BY INDEX]");
        System.out.println("ENTER INDEX: ");
        final Integer index = TerminalUtil.nextNumber() - 1;
        taskService.changeStatusByIndex(index, Status.IN_PROGRESS);
    }

    @Override
    public void completeTaskById() throws AbstractException {
        System.out.println("[COMPLETE TASK BY ID]");
        System.out.println("ENTER ID: ");
        final String id = TerminalUtil.nextLine();
        taskService.changeStatusById(id, Status.COMPLETED);
    }

    @Override
    public void completeTaskByIndex() throws AbstractException {
        System.out.println("[COMPLETE TASK BY INDEX]");
        System.out.println("ENTER INDEX: ");
        final Integer index = TerminalUtil.nextNumber() - 1;
        taskService.changeStatusByIndex(index, Status.COMPLETED);
    }

    @Override
    public void clearTasks() {
        System.out.println("[CLEAR TASK LIST]");
        taskService.clear();
    }

    @Override
    public void removeTasksById() throws AbstractException {
        System.out.println("[REMOVE TASK BY ID]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        taskService.removeById(id);
    }

    @Override
    public void removeTasksByIndex() throws AbstractException {
        System.out.println("[REMOVE TASK BY INDEX]");
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        taskService.removeByIndex(index);
    }

}
